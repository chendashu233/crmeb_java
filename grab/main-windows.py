import time
from decimal import Decimal

import pymysql
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service


def get_driver():
    options = Options()
    options.add_argument("--headless")  # 无界面模式
    service = Service("chromedriver_windows.exe")  # ChromeDriver路径
    return webdriver.Chrome(service=service, options=options)


def get_html(driver):
    driver.get('http://www.idjyj.com/')
    time.sleep(3)
    html = driver.page_source
    driver.quit()
    return BeautifulSoup(html, 'html.parser')


def get_data(soup):
    arr = []
    data = soup.find('span', class_='item-price-gold')
    arr.append(data.text)
    table = soup.find_all('span', class_='item-price')
    for item in table:
        arr.append(item.text)
    arr.append(str(float(arr[4]) + 20))
    return tuple(arr)


def database_operations(price):
    # 连接到MySQL数据库
    conn = pymysql.connect(host='43.143.232.74', user='gold', password='Duodeshi,nbzdds1', db='gold', charset='utf8mb4')
    # 创建一个Cursor:
    cursor = conn.cursor()

    # 查询数据库中最后一条价格记录
    last_price = get_last_price_record(cursor)
    # 运行插入价格记录表语句:
    insert_price_record_sql(cursor, price)
    # 运行修改商品价格语句
    if last_price == float(price[0]):
        print("价格一样，无需修改商品价格")
    else:
        print("价格不一样，开始修改商品价格")
        update_product_value_sql(cursor, price)

    conn.commit()

    # 关闭Cursor和Connection:
    cursor.close()
    conn.close()


def get_last_price_record(cursor):
    # 运行查询需修改价格的商品属性值数据:
    sql = "SELECT * FROM price_record ORDER BY create_time DESC LIMIT 1"
    cursor.execute(sql)
    # 获取所有结果
    results = cursor.fetchall()
    print("上次记录金价为：" + str(results[0][0]))
    return float(results[0][0])


def insert_price_record_sql(cursor, price):
    # 运行插入价格记录表语句:
    sql = "INSERT INTO price_record (gold_price, recycled_gold_price, silver_price," \
          " platinum_price, recycled_au_price, recycled_silver_price, recycled_platinum_price," \
          " au_price) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    cursor.execute(sql, price)
    print("新增一条价格记录数据：")
    print(tuple(price))


def update_product_value_sql(cursor, new_price):
    # 运行查询需修改价格的商品属性值数据:
    sql = "SELECT * FROM eb_store_product_attr_value WHERE is_del = 0 AND is_gold = 1"
    cursor.execute(sql)
    # 获取所有结果
    results = cursor.fetchall()
    # 遍历并打印结果
    for row in results:
        rowid = row[0]
        weight = float(row[13])
        price = weight * float(new_price[0])
        update_sql = "UPDATE eb_store_product_attr_value SET is_del = %s WHERE id = %s"
        # 执行SQL语句，并传入参数
        cursor.execute(update_sql, (1, rowid))
        print("修改id为" + str(rowid) + "的商品属性数据删除状态为1")
        # 运行插入价格记录表语句:
        insert_sql = "INSERT INTO eb_store_product_attr_value (product_id, suk, stock, sales," \
                     " handicraft_cost, can_pay, price, image, `unique`, cost, bar_code, ot_price, weight," \
                     " volume, brokerage, brokerage_two, type, quota, quota_show, attr_value, is_del, is_gold)" \
                     " VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        new_row = list(row[1:])
        for index in range(len(new_row)):
            if is_decimal_type(new_row[index]):
                new_row[index] = float(new_row[index])
        new_row[6] = price
        cursor.execute(insert_sql, tuple(new_row))
        print("新增一条商品属性数据：")
        print(tuple(new_row))
    update_product_sql(cursor)


def is_decimal_type(var):
    return isinstance(var, Decimal)


def update_product_sql(cursor):
    # 运行查询需修改价格的商品数据:
    sql = "SELECT id, price FROM eb_store_product WHERE is_gold = 1"
    cursor.execute(sql)
    product_results = cursor.fetchall()
    for row in product_results:
        value_sql = "SELECT id, price FROM eb_store_product_attr_value " \
                    "WHERE product_id = %s AND is_del = 0 AND is_gold = 1 " \
                    "ORDER BY price LIMIT 1"
        cursor.execute(value_sql, (row[0]))
        product_value_results = cursor.fetchall()
        if product_value_results[0]:
            update_sql = "UPDATE eb_store_product SET price = %s WHERE id = %s"
            # 执行SQL语句，并传入参数
            cursor.execute(update_sql, (product_value_results[0][1], row[0]))
            print("修改id为" + str(row[0]) + "的商品数据价格为" + str(product_value_results[0][1]))


def run():
    driver = get_driver()
    soup = get_html(driver)
    price = get_data(soup)
    database_operations(price)
    current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    print('数据抓取成功！！！' + current_time)


if __name__ == '__main__':
    run()
