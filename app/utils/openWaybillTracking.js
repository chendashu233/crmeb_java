import {
	getWaybillTokenByOrderId
} from '@/api/order.js';
import Cache from '@/utils/cache';
import {
	OPENID
} from '@/config/cache';
var plugin = requirePlugin("logisticsPlugin")
export function toDeliveryPage(orderId) {
	uni.showLoading({
		title: "加载中",
		mask: true
	});
	getWaybillTokenByOrderId(Cache.get(OPENID), orderId).then(res => {
		if (res.data.errCode == "0") {
			plugin.openWaybillTracking({
				waybillToken: res.data.waybill_token
			});
		}
	}).finally(() => {
		uni.hideLoading()
	})
}