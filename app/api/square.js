import request from "@/utils/request.js";

// 选品分类列表
export function squareClassList() {
	return request.get('square/classList', {}, {
		noAuth: true
	});
}

// 选品分类数据新增编辑
export function saveOrUpdateSquareClass(data) {
	return request.post('square/saveOrUpdateClass', data);
}

// 选品分类数据新增编辑
export function queryClassById(id) {
	return request.get('square/queryClassById?id=' + id);
}

// 选品分类删除
export function deleteClass(id) {
	return request.delete('square/deleteClass?id=' + id);
}

// 选品数据新增编辑
export function saveOrUpdateSquare(data) {
	return request.post('square/saveOrUpdate', data);
}

// 选品数据新增编辑
export function squareAllList(data) {
	return request.get('square/allList', data, {
		noAuth: true
	});
}

// 选品数据新增编辑
export function squareListForBuyUser(data) {
	return request.get('square/listForBuyUser', data);
}

// 选品数据查询
export function queryById(id) {
	return request.get('square/queryById?id=' + id);
}

// 选品分类删除
export function deleteSquare(id) {
	return request.delete('square/delete?id=' + id);
}