// 格式化字符串工具
import string from './string.js'
// 获取颜色工具
import color from './color.js'
module.exports = {
  data() {
    
  },
  props: {
    // 背景颜色
    backgroundColor: {
      type: String,
      default: ''
    },
    // 字体颜色
    fontColor: {
      type: String,
      default: ''
    },
    // 字体大小
    fontSize: {
      type: Number,
      default: 0
    },
    // 字体大小单位
    fontUnit: {
      type: String,
      default: 'rpx'
    }
  },
  computed: {
    backgroundColorStyle() {
      return color.getBackgroundColorStyle(this.backgroundColor)
    },
    backgroundColorClass() {
      return color.getBackgroundColorInternalClass(this.backgroundColor)
    },
    fontColorStyle() {
      return color.getFontColorStyle(this.fontColor)
    },
    fontColorClass() {
      return color.getFontColorInternalClass(this.fontColor)
    },
    fontSizeStyle() {
      string.getLengthUnitValue(this.fontSize, this.fontUnit)
    }
  },
  methods: {
    
  }
}