# Popup 弹出层
> 组件名： tn-popup  
> 弹出层容器，用于展示弹窗、信息提示等内容，支持上、下、左、右和中部弹出。组件只提供容器，内部内容由用户自定义。  
# 提示 
> popup弹出层中对应自定义内容的容器中已经内置了scroll-view元素，内如内容超出容器的高度，将会自动获得垂直滚动的特性，如果您因为在slot内容做了滚动的处理，而造成了 冲突的话，请移除自定义关于滚动部分的逻辑。  
# [查看文档](https://vue2.tuniaokj.com/components/popup.html)
## [下载完整示例项目](https://ext.dcloud.net.cn/plugin?name=tuniao-tuniaoui)
## [问题反馈/提出建议](https://vue2.tuniaokj.com/cooperation/about.html)
![](https://vue2.tuniaokj.com/index/index.jpg)