# Select 列选择器
> 组件名： tn-select  
> 此选择器用于单列，多列，多列联动的选择场景。    

# 提示 
> 不建议使用tn-picker组件的单列和多列模式，tn-select组件是专门为列选择而构造的组件，更简单易用。  

# [查看文档](https://vue2.tuniaokj.com/components/select.html)
## [下载完整示例项目](https://ext.dcloud.net.cn/plugin?name=tuniao-tuniaoui)
## [问题反馈/提出建议](https://vue2.tuniaokj.com/cooperation/about.html)
![](https://vue2.tuniaokj.com/index/index.jpg)