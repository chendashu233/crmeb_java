package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.record.PriceRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import java.util.List;

@Mapper
public interface PriceRecordDao extends BaseMapper<PriceRecord> {
    @Select("SELECT * FROM price_record WHERE create_time >= NOW() - INTERVAL 1 DAY")
    List<PriceRecord> selectByLast24Hours();
    @Select("SELECT * FROM price_record WHERE create_time >= NOW() - INTERVAL 1 MONTH")
    List<PriceRecord> selectByLast1Month();
}
