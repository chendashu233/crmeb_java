package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.product.Square;

public interface SquareDao extends BaseMapper<Square> {

}
