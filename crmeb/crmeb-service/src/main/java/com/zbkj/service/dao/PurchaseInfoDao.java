package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.order.PurchaseInfo;
import com.zbkj.common.model.order.StoreOrder;
import com.zbkj.common.request.StoreDateRangeSqlPram;
import com.zbkj.common.request.StoreOrderStaticsticsRequest;
import com.zbkj.common.response.OrderBrokerageData;
import com.zbkj.common.response.StoreOrderStatisticsChartItemResponse;
import com.zbkj.common.response.StoreStaffDetail;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单表 Mapper 接口
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
public interface PurchaseInfoDao extends BaseMapper<PurchaseInfo> {

}
