package com.zbkj.service.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.order.PurchaseInfo;
import com.zbkj.common.response.CommonResult;

public interface PurchaseInfoService extends IService<PurchaseInfo> {
    /**
     * 根据订单id获取代购详情信息
     *
     * @param orderId      类型
     * @return 代购详情信息
     */
    PurchaseInfo getPurchaseInfoByOrderId(String orderId);
    /**
     * 保存代购数据
     *
     * @param request      类型
     * @return 保存代购数据
     */
    CommonResult<String> savePurchaseInfo(PurchaseInfo request);
}
