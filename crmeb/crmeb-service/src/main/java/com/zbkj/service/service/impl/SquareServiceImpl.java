package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.order.StoreOrder;
import com.zbkj.common.model.product.Square;
import com.zbkj.common.model.user.User;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.service.dao.SquareDao;
import com.zbkj.service.service.SquareService;
import com.zbkj.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SquareServiceImpl extends ServiceImpl<SquareDao, Square> implements SquareService {

    @Resource
    private SquareDao squareDao;
    @Autowired
    private UserService userService;


    /**
     * 选品列表
     */
    @Override
    public CommonPage<Square> allList(Integer cid, PageParamRequest pageRequest) {
        PageHelper.startPage(pageRequest.getPage(), pageRequest.getLimit());
        LambdaQueryWrapper<Square> lqw = new LambdaQueryWrapper<>();
        lqw.orderByDesc(Square::getId);
        lqw.eq(Square::getClassId, cid);
        List<Square> list = squareDao.selectList(lqw);
        return CommonPage.restPage(list);
    }

    /**
     * 选品列表-管理列表
     */
    @Override
    public CommonPage<Square> listForManage(Integer cid, PageParamRequest pageRequest) {
        User user = userService.getInfoException();
        PageHelper.startPage(pageRequest.getPage(), pageRequest.getLimit());
        LambdaQueryWrapper<Square> lqw = new LambdaQueryWrapper<>();
        lqw.orderByDesc(Square::getId);
        lqw.eq(Square::getClassId, cid);
        if (user.getRole().equals(2)) {
            lqw.eq(Square::getCreateBy, user.getUid());
        }
        List<Square> list = squareDao.selectList(lqw);
        return CommonPage.restPage(list);
    }
}

