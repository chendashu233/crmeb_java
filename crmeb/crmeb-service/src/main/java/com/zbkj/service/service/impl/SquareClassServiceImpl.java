package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.zbkj.common.model.product.SquareClass;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.service.dao.SquareClassDao;
import com.zbkj.service.service.SquareClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SquareClassServiceImpl extends ServiceImpl<SquareClassDao, SquareClass> implements SquareClassService {

    @Resource
    private SquareClassDao squareClassDao;

    @Autowired
    private SquareClassService squareClassService;

    @Override
    public CommonPage<SquareClass> list(PageParamRequest pageRequest) {
        PageHelper.startPage(pageRequest.getPage(), pageRequest.getLimit());
        LambdaQueryWrapper<SquareClass> lqw = new LambdaQueryWrapper<>();
        lqw.orderByDesc(SquareClass::getId);
        List<SquareClass> list = squareClassDao.selectList(lqw);
        return CommonPage.restPage(list);
    }
}

