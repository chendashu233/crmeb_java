package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.product.SquareClass;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;

public interface SquareClassService extends IService<SquareClass> {

    /**
     * 选品列表
     *
     * @param pageRequest 分页
     * @return 列表集合
     */
    CommonPage<SquareClass> list(PageParamRequest pageRequest);
}
