package com.zbkj.service.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.constants.Constants;
import com.zbkj.common.constants.NotifyConstants;
import com.zbkj.common.constants.UserConstants;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.combination.StorePink;
import com.zbkj.common.model.express.Express;
import com.zbkj.common.model.order.PurchaseInfo;
import com.zbkj.common.model.order.StoreOrder;
import com.zbkj.common.model.sms.SmsTemplate;
import com.zbkj.common.model.system.SystemAdmin;
import com.zbkj.common.model.system.SystemNotification;
import com.zbkj.common.model.system.SystemStore;
import com.zbkj.common.model.user.User;
import com.zbkj.common.model.user.UserBrokerageRecord;
import com.zbkj.common.model.user.UserToken;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.*;
import com.zbkj.common.response.*;
import com.zbkj.common.utils.DateUtil;
import com.zbkj.common.utils.RedisUtil;
import com.zbkj.common.utils.ValidateFormUtil;
import com.zbkj.common.vo.*;
import com.zbkj.service.dao.PurchaseInfoDao;
import com.zbkj.service.dao.StoreOrderDao;
import com.zbkj.service.delete.OrderUtils;
import com.zbkj.service.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * StoreOrderServiceImpl 接口实现
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
@Service
public class PurchaseInfoServiceImpl extends ServiceImpl<PurchaseInfoDao, PurchaseInfo> implements PurchaseInfoService {

    @Autowired
    private PurchaseInfoService purchaseInfoService;
    @Autowired
    private StoreOrderService storeOrderService;

    /**
     * 根据订单id获取代购详情信息
     *
     * @param orderId 类型
     * @return 代购详情信息
     */
    @Override
    public PurchaseInfo getPurchaseInfoByOrderId(String orderId) {
        PurchaseInfo info = this.getById(orderId);
        if (null == info) {
            PurchaseInfo newInfo = new PurchaseInfo();
            newInfo.setOrderId(orderId);
            this.save(newInfo);
            return newInfo;
        }
        return info;
    }

    @Override
    public CommonResult<String> savePurchaseInfo(PurchaseInfo request) {
        if (purchaseInfoService.updateById(request)) {
            LambdaUpdateWrapper<StoreOrder> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(StoreOrder::getOrderId, request.getOrderId()).set(StoreOrder::getDeliveryId, request.getExpressNumber());
            if (storeOrderService.update(updateWrapper)) {
                return CommonResult.success();
            } else {
                return CommonResult.failed();
            }
        } else {
            return CommonResult.failed();
        }
    }
}

