package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.record.PriceRecord;
import com.zbkj.service.dao.PriceRecordDao;
import com.zbkj.service.dao.UserAddressDao;
import com.zbkj.service.service.PriceRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PriceRecordServiceImpl extends ServiceImpl<PriceRecordDao, PriceRecord> implements PriceRecordService {

    @Resource
    private PriceRecordDao priceRecordDao;

    @Override
    public PriceRecord getNewRecord() {
        QueryWrapper<PriceRecord> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("*")
                .orderByDesc("create_time")
                .last("LIMIT 1");
        return priceRecordDao.selectOne(queryWrapper);
    }

    @Override
    public List<PriceRecord> record24h(){
        return priceRecordDao.selectByLast24Hours();
    }

    @Override
    public List<PriceRecord> record1Month(){
        return priceRecordDao.selectByLast1Month();
    }
}

