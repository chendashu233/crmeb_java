package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.product.Square;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;

public interface SquareService extends IService<Square> {

    /**
     * 选品列表
     *
     * @param cid 分类id
     * @param pageRequest 分页
     * @return 列表集合
     */
    CommonPage<Square> allList(Integer cid, PageParamRequest pageRequest);

    /**
     * 选品列表-管理列表
     *
     * @param cid 分类id
     * @param pageRequest 分页
     * @return 列表集合
     */
    CommonPage<Square> listForManage(Integer cid, PageParamRequest pageRequest);
}
