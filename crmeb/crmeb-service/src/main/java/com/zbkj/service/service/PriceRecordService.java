package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.record.PriceRecord;
import com.zbkj.common.request.PageParamRequest;

import java.util.List;

public interface PriceRecordService extends IService<PriceRecord> {
    /**
     * 获取实时价格记录
     * @return PriceRecord
     */
    PriceRecord getNewRecord();

    /**
     * 24h价格记录
     * @return List<PriceRecord>
     */
    List<PriceRecord> record24h();

    /**
     * 1月价格记录
     * @return List<PriceRecord>
     */
    List<PriceRecord> record1Month();

}
