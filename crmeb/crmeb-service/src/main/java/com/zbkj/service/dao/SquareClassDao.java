package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.product.SquareClass;

public interface SquareClassDao extends BaseMapper<SquareClass> {

}
