package com.zbkj.front.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zbkj.common.model.product.Square;
import com.zbkj.common.model.product.SquareClass;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.StoreNearRequest;
import com.zbkj.common.request.UserCollectRequest;
import com.zbkj.common.response.CommonResult;
import com.zbkj.common.response.StoreNearResponse;
import com.zbkj.service.service.SquareClassService;
import com.zbkj.service.service.SquareService;
import com.zbkj.service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController("SquareController")
@RequestMapping("api/front/square")
@Api(tags = "选品")
public class SquareController {

    @Autowired
    private SquareService squareService;
    @Autowired
    private SquareClassService squareClassService;
    @Autowired
    private UserService userService;

    /**
     * 选品分类列表
     */
    @ApiOperation(value = "选品分类列表")
    @RequestMapping(value = "/classList", method = RequestMethod.GET)
    public CommonResult<List<SquareClass>> list() {
        QueryWrapper<SquareClass> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("sort_index");
        return CommonResult.success(squareClassService.list(queryWrapper));
    }


    /**
     * 选品分类数据新增编辑
     */
    @ApiOperation(value = "选品分类数据新增编辑")
    @RequestMapping(value = "/saveOrUpdateClass", method = RequestMethod.POST)
    public CommonResult<String> saveOrUpdateClass(@RequestBody @Validated SquareClass squareClass) {
        if (null == squareClass.getId()) {
            Integer userId = userService.getUserIdException();
            squareClass.setCreateBy(userId);
        }
        if (squareClassService.saveOrUpdate(squareClass)) {
            return CommonResult.success();
        } else {
            return CommonResult.failed();
        }
    }


    /**
     * 选品分类查询
     */
    @ApiOperation(value = "选品分类查询")
    @RequestMapping(value = "/queryClassById", method = RequestMethod.GET)
    public CommonResult<SquareClass> queryClassById(@RequestParam(name = "id") Integer id) {
        return CommonResult.success(squareClassService.getById(id));
    }

    /**
     * 选品分类删除
     */
    @ApiOperation(value = "选品分类删除")
    @RequestMapping(value = "/deleteClass", method = RequestMethod.DELETE)
    public CommonResult<String> deleteClass(@RequestParam(name = "id") Integer id) {
        if (squareClassService.removeById(id)) {
            return CommonResult.success();
        } else {
            return CommonResult.failed();
        }
    }

    /**
     * 选品列表
     */
    @ApiOperation(value = "选品列表")
    @RequestMapping(value = "/allList", method = RequestMethod.GET)
    public CommonResult<CommonPage<Square>> allList(@RequestParam(name = "cid") Integer cid, @ModelAttribute PageParamRequest pageRequest) {
        return CommonResult.success(squareService.allList(cid, pageRequest));
    }


    /**
     * 选品列表-管理列表
     */
    @ApiOperation(value = "选品列表-管理列表")
    @RequestMapping(value = "/listForBuyUser", method = RequestMethod.GET)
    public CommonResult<CommonPage<Square>> listForBuyUser(@RequestParam(name = "cid") Integer cid, @ModelAttribute PageParamRequest pageRequest) {
        return CommonResult.success(squareService.listForManage(cid, pageRequest));
    }


    /**
     * 选品数据新增编辑
     */
    @ApiOperation(value = "选品数据新增编辑")
    @RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
    public CommonResult<String> saveOrUpdate(@RequestBody @Validated Square square) {
        if (null == square.getId()) {
            Integer userId = userService.getUserIdException();
            square.setCreateBy(userId);
        }
        if (squareService.saveOrUpdate(square)) {
            return CommonResult.success();
        } else {
            return CommonResult.failed();
        }
    }

    /**
     * 选品数据删除
     */
    @ApiOperation(value = "选品数据删除")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public CommonResult<String> delete(@RequestParam(name = "id") Integer id) {
        if (squareService.removeById(id)) {
            return CommonResult.success();
        } else {
            return CommonResult.failed();
        }
    }

    /**
     * 选品查询
     */
    @ApiOperation(value = "选品查询")
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public CommonResult<Square> queryById(@RequestParam(name = "id") Integer id) {
        return CommonResult.success(squareService.getById(id));
    }
}