package com.zbkj.front.controller;

import com.zbkj.common.model.record.PriceRecord;
import com.zbkj.common.response.CommonResult;
import com.zbkj.service.service.PriceRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Slf4j
@RestController("PriceRecordController")
@RequestMapping("api/front/price")
@Api(tags = "价格记录")
public class PriceRecordController {

    @Autowired
    private PriceRecordService priceRecordService;

    /**
     * 最新价格记录
     */
    @ApiOperation(value = "最新价格记录")
    @RequestMapping(value = "/newRecord", method = RequestMethod.GET)
    public CommonResult<PriceRecord> newRecord() {
        return CommonResult.success(priceRecordService.getNewRecord());
    }

    /**
     * 24h价格记录
     */
    @ApiOperation(value = "24h价格记录")
    @RequestMapping(value = "/record24h", method = RequestMethod.GET)
    public CommonResult<List<PriceRecord>> record24h() {
        return CommonResult.success(priceRecordService.record24h());
    }

    /**
     * 1月价格记录
     */
    @ApiOperation(value = "1月价格记录")
    @RequestMapping(value = "/record1Month", method = RequestMethod.GET)
    public CommonResult<List<PriceRecord>> record1Month() {
        return CommonResult.success(priceRecordService.record1Month());
    }


}



