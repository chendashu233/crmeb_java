package com.zbkj.common.model.record;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("price_record")
@ApiModel(value="金价记录对象", description="金价记录表")
public class PriceRecord implements Serializable {

    @ApiModelProperty(value = "黄金价格")
    private String goldPrice;

    @ApiModelProperty(value = "黄金回收价格")
    private String recycledGoldPrice;

    @ApiModelProperty(value = "AU750价格")
    private String auPrice;

    @ApiModelProperty(value = "AU750回收价格")
    private String recycledAuPrice;

    @ApiModelProperty(value = "铂金价格")
    private String platinumPrice;

    @ApiModelProperty(value = "铂金回收价格")
    private String recycledPlatinumPrice;

    @ApiModelProperty(value = "白银价格")
    private String silverPrice;

    @ApiModelProperty(value = "白银回收价格")
    private String recycledSilverPrice;

    @ApiModelProperty(value = "记录时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp createTime;
}
