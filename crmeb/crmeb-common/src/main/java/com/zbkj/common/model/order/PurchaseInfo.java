package com.zbkj.common.model.order;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("purchase_info")
@ApiModel(value="purchase_info对象", description="代购详情")
public class PurchaseInfo {

    @ApiModelProperty(value = "订单ID")
    @TableId
    private String orderId;

    @ApiModelProperty(value = "商户门头照")
    private String doorPhoto;

    @ApiModelProperty(value = "商品照片（最多三张）")
    private String goodsPhoto;

    @ApiModelProperty(value = "商品细节视频")
    private String goodsVideo;

    @ApiModelProperty(value = "鉴定机构门头照")
    private String institutionDoorPhoto;

    @ApiModelProperty(value = "鉴定结果照片")
    private String identifyPhoto;

    @ApiModelProperty(value = "包装照片")
    private String packagePhoto;

    @ApiModelProperty(value = "发货照片")
    private String shippingPhoto;

    @ApiModelProperty(value = "代购备注")
    private String remark;

    @ApiModelProperty(value = "快递单号")
    @TableField(exist = false)
    private String expressNumber;
}
