package com.zbkj.common.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 微信小程序手机号授权返回数据
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="WeChatPhoneVo对象", description="微信小程序手机号授权返回数据")
public class WeChatPhoneVo implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "会话密钥")
    private WeChatPhoneInfoVo phone_info;

    @ApiModelProperty(value = "错误码")
    private String errCode;

    @ApiModelProperty(value = "错误信息")
    private String errMsg;
}
