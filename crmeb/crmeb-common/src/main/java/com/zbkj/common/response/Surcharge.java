package com.zbkj.common.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="订单附加金额", description="订单附加金额")
public class Surcharge {
    @ApiModelProperty(value = "代购费")
    private BigDecimal purchaseFee;
    @ApiModelProperty(value = "鉴定费")
    private BigDecimal appraisalFee;
    @ApiModelProperty(value = "包装费")
    private BigDecimal packingCharges;
}
